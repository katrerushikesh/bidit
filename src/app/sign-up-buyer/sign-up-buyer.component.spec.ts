import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpBuyerComponent } from './sign-up-buyer.component';

describe('SignUpBuyerComponent', () => {
  let component: SignUpBuyerComponent;
  let fixture: ComponentFixture<SignUpBuyerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpBuyerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpBuyerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
