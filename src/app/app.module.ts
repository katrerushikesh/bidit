import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule, routingComponents} from './app-routing.module';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { PageNotComponent } from './page-not/page-not.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { SignUpSellerComponent } from './sign-up-seller/sign-up-seller.component';
import { SignUpBuyerComponent } from './sign-up-buyer/sign-up-buyer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BuyerComponent } from './buyer/buyer.component';
import { SellerComponent } from './seller/seller.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    routingComponents,
    HomeComponent,
    PageNotComponent,
    ContactUsComponent,
    SignUpSellerComponent,
    SignUpBuyerComponent,
    DashboardComponent,
    BuyerComponent,
    SellerComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
