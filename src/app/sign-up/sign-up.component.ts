import { Component, OnInit } from '@angular/core';

import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  buyerfname;
  buyerlname;
  buyerphone;
  buyerpassword;
  buyeradhaar;
  buyeremail;

  sellerfname;
  sellerlname;
  sellerphone;
  sellerpassword;
  selleradhaar;
  sellergstin;
  sellerdomain;
  selleremail;

  b_invalid = false;
  invalid = false;

  constructor(private http:Http) { }

  ngOnInit() {
  }
  normalFormSubmit()
  {
    this.isTaken(this.buyeremail)
    if(this.validateNormalForm() && this.isTaken(this.buyeremail))
    {
      this.http.post('http://localhost/Bidit_php/NormalSubmit.php',{body:{'fname':this.buyerfname,
                                                                        'lname':this.buyerlname,
                                                                        'email':this.buyeremail,
                                                                        'phone':this.buyerphone,
                                                                        'password':this.buyerpassword,
                                                                        'adhaar':this.buyeradhaar}}).subscribe
      (
       response=>{
          console.log(response.text());
        },
        error=>{

        }
      );
    }
  }

  businessFormSubmit()
  {
    if(this.validateBusinessForm() && this.isTaken(this.selleremail))
    {
      this.http.post('http://localhost/Bidit_php/BusinessSubmit.php',{body:{'fname':this.sellerfname,
                                                                          'lname':this.sellerlname,
                                                                          'email':this.selleremail,
                                                                          'phone':this.sellerphone,
                                                                          'password':this.sellerpassword,
                                                                          'adhaar':this.selleradhaar,
                                                                          'gstin':this.sellergstin,
                                                                          'domain':this.sellerdomain}}).subscribe
      (
        response=>{
          console.log(response.text());
        },
        error=>{

        }
      );
    }
   }

  
  isTaken(email)
   {
      this.http.post('http://localhost/Bidit_php/IsTaken.php',{body:{'email':email}}).subscribe
      (
        response=>{
          console.log(response.text());
        },
        error=>{

        }
      );
      return true;
   }
  
   validateNormalForm()
  {
    if(this.buyerfname == undefined )
    {
      this.invalid = true;
      return false;
    }
    else if(this.buyerlname == undefined)
    {
      this.invalid = true;
      return false;
    }
    else if(this.buyeremail == undefined)
    {
      this.invalid = true;
      return false;
    }
    else if(this.buyerphone == undefined)
    {
      this.invalid = true;
      return false;
    }
    else if(this.buyerpassword == undefined)
    {
      this.invalid = true;
      return false;
    }
    else if(this.buyeradhaar == undefined)
    {
      this.invalid = true;
      return false;
    }
    else
    {
      return true;
    }
  
  }

  validateBusinessForm()
  {
    if(this.sellerfname == undefined )
    {
      this.b_invalid = true;
      return false;
    }
    else if(this.sellerlname == undefined)
    {
      this.b_invalid = true;
      return false;
    }
    else if(this.selleremail == undefined)
    {
      this.b_invalid = true;
      return false;
    }
    else if(this.sellerphone == undefined)
    {
      this.b_invalid = true;
      return false;
    }
    else if(this.sellerpassword == undefined)
    {
      this.b_invalid = true;
      return false;
    }
    else if(this.selleradhaar == undefined)
    {
      this.b_invalid = true;
      return false;
    }
    if(this.sellergstin == undefined )
    {
      this.b_invalid = true;
      return false;
    }
    else if(this.sellerdomain == undefined)
    {
      this.b_invalid = true;
      return false;
    }
    else
    {
      return true;
    }
  
  }
 

  

}


