import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PageNotComponent } from './page-not/page-not.component';
import { SignUpBuyerComponent } from './sign-up-buyer/sign-up-buyer.component';
import { SignUpSellerComponent } from './sign-up-seller/sign-up-seller.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BuyerComponent } from './buyer/buyer.component';
import { SellerComponent } from './seller/seller.component';

const routes: Routes = [
  {path : '' , component: HomeComponent},
  {path : 'home' , component: HomeComponent},
  {path : 'sign-up', component: SignUpComponent },
  {path : 'sign-in', component: SignInComponent },
  {path : 'contact', component: ContactUsComponent},
  {path : 'sign-up-buyer', component: SignUpBuyerComponent },
  {path : 'sign-up-seller', component: SignUpSellerComponent},
  {path : 'dashboard', component: DashboardComponent},
  {path : 'seller', component: SellerComponent},
  {path : 'buyer', component: BuyerComponent},
  {path : '**', component: PageNotComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [SignUpComponent, SignInComponent]
